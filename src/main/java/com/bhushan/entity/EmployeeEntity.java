package com.bhushan.entity;

import java.io.Serializable;

import javax.persistence.*;;

@Entity
@Table(name = "employee")
public class EmployeeEntity implements Serializable {

	@Id
	@Column(name = "user_id" , unique = true)
	private int userId;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "role")
	private String role;

	@Column(name = "adharno")
	private long adharno;

	@Column(name = "pancardno")
	private String pancardno;

	@Column(name = "joineddate")
	private String joineddate;

	@Column(name = "mobileno")
	private long mobileno;

	public EmployeeEntity(int userId, String userName, String password, String role, long adharno, String pancardno,
			String joineddate, long mobileno) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.role = role;
		this.adharno = adharno;
		this.pancardno = pancardno;
		this.joineddate = joineddate;
		this.mobileno = mobileno;
	}

	public EmployeeEntity() {
		super();
	}

	public EmployeeEntity(int userId, String userName, String password) {
		super();

		this.userName = userName;
		this.password = password;
		this.userId = userId;

	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public long getAdharno() {
		return adharno;
	}

	public void setAdharno(long adharno) {
		this.adharno = adharno;
	}

	public String getPancardno() {
		return pancardno;
	}

	public void setPancardno(String pancardno) {
		this.pancardno = pancardno;
	}

	public String getJoineddate() {
		return joineddate;
	}

	public void setJoineddate(String joineddate) {
		this.joineddate = joineddate;
	}

	public long getMobileno() {
		return mobileno;
	}

	public void setMobileno(long mobileno) {
		this.mobileno = mobileno;
	}
}
