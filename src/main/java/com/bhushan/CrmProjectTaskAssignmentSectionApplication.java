package com.bhushan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class CrmProjectTaskAssignmentSectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmProjectTaskAssignmentSectionApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate;
	}
	@Bean
	RestOperations rest(RestTemplateBuilder restTemplateBuilder) {
	    return restTemplateBuilder.basicAuthentication("bhushan", "magical").build();
	}
}
