package com.bhushan.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.entity.EmployeeEntity;
import com.bhushan.entity.TaskEntity;
import com.bhushan.response.EmployeeResponse;
import com.bhushan.response.Response;
import com.bhushan.service.TaskService;

@RestController
@CrossOrigin
public class TaskController {

	@Autowired
	private TaskService taskservice;

	@PostMapping("/createTask")
	public Response createtask(@RequestBody TaskEntity taskEntity) throws Exception {
		return taskservice.saveTask(taskEntity);

	}

	@GetMapping("/getbyTaskno/{taskno}")
	public Response getTaskbyId(@PathVariable int taskno) {
		return taskservice.getTaskbyId(taskno);
				
	}
	@PostMapping("/editTask")
	public Response editTask(@RequestBody TaskEntity taskEntity) throws Exception {
		Response editTask = taskservice.editTask(taskEntity);
		return editTask;
	}
	@GetMapping("/gettaskbyemid/{user_id}")
	public TaskEntity getTaskbyuserId(@PathVariable int user_id) {
		TaskEntity findbyuser_id = taskservice.getTaskbyuserId(user_id);
		return findbyuser_id;
	}

	@GetMapping("/getAllTask")
	public List<TaskEntity> getAllTask() throws Exception {
		return taskservice.findAllUsers();

	}
	@PostMapping("/updateEmployee/{taskno}")
	public Response updateUser(@PathVariable int taskno , @RequestBody TaskEntity entity) throws Exception {
		Response updateUser = taskservice.updateUser(taskno, entity);
		return updateUser;
	}
	

	@PostMapping("/createEmployee")
	public EmployeeResponse createEmployee(@RequestBody EmployeeEntity taskEntity) throws Exception {
		return taskservice.saveEmployee(taskEntity);

	}

	@PostMapping("/deleteTask/{taskno}")
	public Response deleteTask(@PathVariable int taskno) throws Exception {
		return taskservice.deleteUser(taskno);

	}

	@GetMapping("/getDatabyRange/{empid}/{start}/{end}")
	public List<TaskEntity> getDatabyDateRange(@PathVariable Integer empid, @PathVariable String start,
			@PathVariable String end) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = formatter.parse(start);
		Date dates = formatter.parse(end);
		Timestamp timeStampDate = new Timestamp(date.getTime());
		Timestamp timeStampDates = new Timestamp(dates.getTime());
		List<TaskEntity> findTaskDescriptionBetweenDate = taskservice.getDatabyDateRange(empid, timeStampDate,
				timeStampDates);
		return findTaskDescriptionBetweenDate;
	}

	@GetMapping("/getTaskById/{empid}")
	public List<TaskEntity> findTaskForEmployeeId(@PathVariable Integer empid) {
		List<TaskEntity> findTaskForEmployeeId = taskservice.findTaskForEmployeeId(empid);
		return findTaskForEmployeeId;
	}

	@GetMapping("/employeeGetTaskById/{id}")
	public TaskEntity findTaskforemployee(@PathVariable Integer id) {
		Optional<TaskEntity> findUserById = taskservice.findUserById(id);
		return findUserById.get();
	}

	@GetMapping("/employeeGetTaskById/{id}/{status}")
	public TaskEntity setStatus(@PathVariable Integer id, @PathVariable String status) throws Exception {
		TaskEntity saveStatus = taskservice.saveStatus(id, status);
		return saveStatus;
	}

}
