package com.bhushan.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.entity.AddingProductForView;
import com.bhushan.entity.EmployeeEntity;
import com.bhushan.entity.TaskEntity;
import com.bhushan.repo.EmployeeRepo;
import com.bhushan.repo.TaskRepo;
import com.bhushan.repo.ViewProductRepo;
import com.bhushan.response.EmployeeResponse;
import com.bhushan.response.Response;
import com.bhushan.resttemplate.RestTemplatesServices;

import Enumeration.HTTPStatus;

@Service
public class TaskService {

	@Autowired
	private TaskRepo employeerepo;

	@Autowired
	private EmployeeRepo employeerepos;

	@Autowired
	private ViewProductRepo viewProductRepo;

	@Autowired
	private RestTemplatesServices restTemplatesServices;

	public List<TaskEntity> findAllUsers() throws Exception {
		List<TaskEntity> findAll = employeerepo.findAll();
		return findAll;
	}

	public Optional<TaskEntity> findUserById(int id) {
		return employeerepo.findById(id);
	}

	public Response getTaskbyId(int taskno) {
		Response response = new Response();
		Optional<TaskEntity> findTaskno = employeerepo.findById(taskno);
		if (findTaskno.isPresent()) {
			TaskEntity taskEntity = findTaskno.get();
			response.setEmployeepanel(taskEntity);
			response.setMessage("Task has been Retrieved");
			response.setStatus(200);
		} else {
			response.setMessage("No Task is not Present");
			response.setStatus(500);
		}
		return response;
	}

	public Response editTask(TaskEntity customer) throws Exception {
		Response response = new Response();
		customer.setDate("2001-06-21");
		customer.setTaskdate("2001-06-21");
		Optional<EmployeeEntity> employee = employeerepos.findById(customer.getEmployee().getUserId());
		Optional<TaskEntity> findById = employeerepo.findById(customer.getTaskno());
		if (employee.isPresent() && findById.isPresent()) {
			TaskEntity employeeEntity = findById.get();
			customer.setEmployee(employeeEntity.getEmployee());
			customer.setTenure(customer.getTenure());
			customer.setStatus(customer.getStatus());
			customer.setTaskdate(employeeEntity.getTaskdate());
			customer.setTaskdescription(employeeEntity.getTaskdescription());
			customer.setDate(employeeEntity.getDate());
			TaskEntity customerData = employeerepo.save(customer);
			response.setMessage("Data has been saved");
			response.setStatus(200);
			response.setEmployeepanel(customerData);
			return response;
		} else {
			response.setStatus(500);
			response.setMessage("Something went Wrong");
		}
		return response;

	}

	public Response saveTask(TaskEntity customer) throws Exception {
		Response response = new Response();
		customer.setDate("2001-06-21");
		customer.setTaskdate("2001-06-21");
		Optional<EmployeeEntity> employee = employeerepos.findById(customer.getEmployee().getUserId());
		if (employee.isPresent()) {
			EmployeeEntity employeeEntity = employee.get();
			customer.setEmployee(employeeEntity);
			TaskEntity customerData = employeerepo.save(customer);
			response.setMessage("Data has been saved");
			response.setStatus(200);
			response.setEmployeepanel(customerData);
			return response;
		} else {
			response.setStatus(500);
			response.setMessage("Something went Wrong");
		}
		return response;

	}

	public HTTPStatus saveViewProduct(AddingProductForView productForView) throws Exception {
		Optional<AddingProductForView> viewProduct = viewProductRepo.findById(productForView.getProductid());
		if (viewProduct.isPresent()) {
			AddingProductForView employeeEntity = viewProduct.get();
			AddingProductForView customerData = viewProductRepo.save(employeeEntity);
			return HTTPStatus.HTTP_STATUS_OK;
		} else {
			return HTTPStatus.HTTP_INTERNALSERVER_ERROR;
		}
	}

	public TaskEntity getTaskbyuserId(int user_id) {
		TaskEntity findbyuser_id = employeerepo.findbyuser_id(user_id);
		return findbyuser_id;
	}

	public EmployeeResponse saveEmployee(EmployeeEntity customer) throws Exception {
		EmployeeResponse response = new EmployeeResponse();
		EmployeeEntity customerData = employeerepos.save(customer);
		response.setMessage("Data has been saved");
		response.setStatus(200);
		response.setEmployeepanel(customerData);
		return response;
	}

	public List<TaskEntity> getDatabyDateRange(Integer pointno, Timestamp start, Timestamp end) {
		List<TaskEntity> findTaskDescriptionBetweenDate = employeerepo.findTaskDescriptionBetweenDate(pointno, start,
				end);
		return findTaskDescriptionBetweenDate;
	}

	public Response updateUser(int id, TaskEntity customer) throws Exception {
		Optional<TaskEntity> retrievedUser = employeerepo.findById(id);
		Response response = new Response();
		if (retrievedUser.isPresent()) {
			employeerepo.save(customer);
			response.setMessage("Data has been saved");
			response.setStatus(200);
			response.setEmployeepanel(customer);
			return response;
		} else {
			throw new Exception("User not found");
		}
	}

	public TaskEntity saveStatus(Integer userId, String status) throws Exception {
		Optional<TaskEntity> retrievedUser = employeerepo.findById(userId);
		if (retrievedUser == null)
			try {
				throw new Exception("User not found");
			} catch (Exception e) {
				e.printStackTrace();
			}
		int setStatus = employeerepo.updatestatus(status, userId);
		System.out.println(setStatus);
		if (setStatus == 1) {
			return retrievedUser.get();
		} else {
			throw new Exception("something went wrong");
		}

	}

	public Response deleteUser(int userId) {
		Response response = new Response();
		Optional<TaskEntity> retrievedUser = employeerepo.findById(userId);
		if (retrievedUser.isPresent()) {
			employeerepo.deleteById(userId);
			response.setStatus(200);
			response.setMessage("Employee deleted Successfully");
		} else {

			response.setStatus(500);
			response.setMessage("Something went wrong");
		}
		return response;

	}

	public List<TaskEntity> findTaskForEmployeeId(Integer empid) {
		List<TaskEntity> findTaskForEmployeeId = employeerepo.findTaskForEmployeeId(empid);
		return findTaskForEmployeeId;
	}

}
