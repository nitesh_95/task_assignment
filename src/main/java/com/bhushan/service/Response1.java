package com.bhushan.service;

import com.bhushan.entity.EmployeeEntity;
import com.bhushan.entity.TaskEntity;

public class Response1 {


	private int status;
	private String message;
	private EmployeeEntity employeepanel;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public EmployeeEntity getEmployeepanel() {
		return employeepanel;
	}

	public void setEmployeepanel(EmployeeEntity employeepanel) {
		this.employeepanel = employeepanel;
	}


}
