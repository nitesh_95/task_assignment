package com.bhushan.resttemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestTemplatesServices {

	@Autowired
	private RestTemplate restTemplate;

	public String getemployees(Integer empid) throws ParseException {
		String url = "http://localhost:8090/v1/usersData/" + empid;
		String username = "radhe";
		String password = "magical";
		HttpHeaders headers = new HttpHeaders();
		headers.setBasicAuth(username, password);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> httpEntity = new HttpEntity<String>(headers);
		Map<String, Integer> map = new HashMap<>();
		map.put("userId", empid);
		String body = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class).getBody();
		return body;

	}

}
