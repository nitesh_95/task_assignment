package com.bhushan.response;

import com.bhushan.entity.TaskEntity;

public class Response {


	private int status;
	private String message;
	private TaskEntity employeepanel;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TaskEntity getEmployeepanel() {
		return employeepanel;
	}

	public void setEmployeepanel(TaskEntity employeepanel) {
		this.employeepanel = employeepanel;
	}


}
