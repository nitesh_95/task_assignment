package com.bhushan.repo;

import java.sql.Timestamp;
import java.util.List;

import org.aspectj.weaver.tools.Trace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.entity.TaskEntity;

@Repository
public interface TaskRepo extends JpaRepository<TaskEntity, Integer> {

	@Query(value = "select * from task_data where empid =?1 and  date between ?2 AND ?3 order by taskdate desc ", nativeQuery = true)
	public List<TaskEntity> findTaskDescriptionBetweenDate(@Param("empid") Integer empid,
			@Param("startDate") Timestamp startDate, @Param("endDate") Timestamp endDate);

	@Query(value = "select * from task_data where empid =?1 ", nativeQuery = true)
	public List<TaskEntity> findTaskForEmployeeId(@Param("empid") Integer empid);

	@Query(value =  "select * from task_data where user_id = ?1" , nativeQuery = true)
	TaskEntity findbyuser_id(@Param("user_id") Integer user_id);

	@Modifying
	@org.springframework.transaction.annotation.Transactional
	@Query(value = "UPDATE task_data SET status = ?1 WHERE taskno = ?2", nativeQuery = true)
	int updatestatus(@Param("status") String status, @Param("id") Integer id);

}
