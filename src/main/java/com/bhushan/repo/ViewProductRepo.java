package com.bhushan.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bhushan.entity.AddingProductForView;

public interface ViewProductRepo extends JpaRepository<AddingProductForView, Integer> {

}
